/*
 
   Copyright 2021 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/


/*
 
  "Directory listing" "." "ls -lah"

  Example output

  --
  Hello world... Success (21ms)
  - fastcc helloworld.fast && ./hello_world

 */


// Note: buffer is resuable
void sim(struct STRING *name, 
         struct STRING *path, struct STRING *command, struct BUFF_FIXED *buff) {
  

  // reset the buffer
  buff->used    = 0;

  // time and run the command
  u64 start     = sys$timer();
  u32 exit_code = sys$exec(path, command, buff);
  u64 stop      = sys$timer();


  // print the output
  printf("--\n");
  printf("%s... ", string$to_c_string(name));
  if(exit_code == 0) {
    printf("Success (%lu millis)\n", stop - start);
  } else {
    printf("Failed (%lu millis)\n", stop - start);
  } 
  printf("command `%s` output: \n", string$to_c_string(command));
  printf("%s\n", buff->data);
} 


// Note: buffer is resuable
// TODO add test
void sim_validate(struct STRING *name, 
                  struct STRING *path, struct STRING *command, 
                  struct BUFF_FIXED *buff, struct STRING *compare) {
  // reset the buffer
  buff->used    = 0;

  // time and run the command
  u64 start     = sys$timer();
  u32 exit_code = sys$exec(path, command, buff);
  u64 stop      = sys$timer();


  // print the output
  printf("--\n");
  printf("%s... ", string$to_c_string(name));
  if(exit_code == 0) {
    if(strcmp(buff->data, string$to_c_string(compare)) == 0) {
      printf("Success (%lu millis)\n", stop - start);
    } else {
      printf("Failed validate: expected `%s`, found `%s` (%lu millis)\n", string$to_c_string(compare),buff->data, stop - start);
    }
  } else {
    printf("Failed with exit code: %d (%lu millis)\n", exit_code, stop - start);
  } 
  printf("output: \n%s\n", buff->data);
  printf("- %s\n", string$to_c_string(command));

}
  


/*
s32 process_file(struct CTX *ctx, FILE *file_handle) {

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  bool failed;

  struct STRING *words[10];
  u32    word_count;

  while ((read = getline(&line, &len, file_handle)) != -1) { 
    word_count = string$split_to_array(line, words);
    failed = config_handle_line(ctx, line, words, word_count);
    if(failed == true) {
      exit(1); // TODO horrible error handling, fix so it bubbles up
    }
  }

  return 1;
}



s32 read_config(struct CTX *ctx, char *path) { 
  // TODO error handling when path isn't found 

  // file open
  FILE *file_handle = fopen(path, "r"); 

  if(file_handle == NULL) {
    printf("FILE NOT FOUND, read_config: %s", path);
    return -1;
  } 

  // validate file path 
  process_file(ctx, file_handle);
  	
  // file close	
  fclose(file_handle);

  return 0;
}


*/

void process_sim_file(FILE *file_handle) {
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  bool failed;

  u32 max_tokens = 4;
  struct STRING **tokens = malloc(max_tokens * sizeof(struct STRING *));
  struct BUFF_FIXED *buff_fixed = buff$make_fixed(1000);

  u32    word_count;

  while ((read = getline(&line, &len, file_handle)) != -1) { 
   
   u32 token_count = string$split_to_array_with_quoted(line, tokens, max_tokens);

   // if we have the right amount of tokens we can the command
    if(token_count == 3) {
      sim(tokens[0], tokens[1], tokens[2], buff_fixed);
    } else if(token_count == 4) {
      string$escaped_to_special(tokens[3]);
      sim_validate(tokens[0], tokens[1], tokens[2], buff_fixed, tokens[3]);
    } else {
      printf("invalid line %d items found, 3-4 items required name path command, [validate] use quoted strings if needed, line: %s\n", token_count, line);
    }
  }
}


u32 sim_run(struct STRING *path) {
  // ******** Load sims to run
  
  // open file
  FILE *file_handle = fopen(string$to_c_string(path), "r"); 
  
  if(file_handle == NULL) {
    printf("FILE NOT FOUND, could not load sim file: %s", string$to_c_string(path));
    return -1;
  } 

  // validate file path 
  process_sim_file(file_handle);

  // close file
  fclose(file_handle);

  return 0;

} 


int main(int argc, char **argv) {
  struct STRING path;
  string$from_c_string(&path, "sim.world");

  sim_run(&path);

  return 0;
}


// TESTS ---------------------------------------------


u32 sim_test$sim() {
  struct STRING     *name     = string$make();
  struct STRING     *path     = string$make();
  struct STRING     *command  = string$make();
  struct BUFF_FIXED *buff     = buff$make_fixed(10000);

  string$append_chars(name,    "sim test");
  string$append_chars(path,    "~/work");
  string$append_chars(command, "ls -lah");
  printf("\n");
  sim(name, path, command, buff); 

  return 0;
}


u32 sim_test$sim_run() {
  struct STRING path;

  string$from_c_string(&path, "sim.world");
  
  sim_run(&path);

  return 0;

}

